from dash import Dash, dcc, html, Input, Output
import plotly.express as px
import pandas as pd
import argparse
import os
import plotly.graph_objects as go


app = Dash(__name__)

root_dir = os.getcwd()
root_dir_runs = root_dir + "/runs/"
# 1 epochs 11_r, 11_i 20 epochs, 11_p 5 epochs, 11_q 10 epochs

path_root_exp11i = os.path.join(root_dir_runs, "exp11i", 'plot_data.csv') 
path_root_exp11p = os.path.join(root_dir_runs, "exp11p", 'plot_data.csv')
path_root_exp11q = os.path.join(root_dir_runs, "exp11q", 'plot_data.csv')
path_root_exp11r = os.path.join(root_dir_runs, "exp11r", 'plot_data.csv')

app.layout = html.Div([
    html.H4('Learn by doing: plot experiment'),
    dcc.Graph(id="graph")
])

@app.callback(
    Output("graph", "figure"), 
    Input("graph", "figure"))
def update_line_chart(self):
    df_plot_values_exp11i = pd.read_csv(path_root_exp11i)
    df_plot_values_exp11p = pd.read_csv(path_root_exp11p)
    df_plot_values_exp11q = pd.read_csv(path_root_exp11q)
    df_plot_values_exp11r = pd.read_csv(path_root_exp11r)
    # fig = px.line(x=df_plot_values["Unnamed: 0"], y=[df_plot_values["Pred_acc"], df_plot_values["Gen_acc"], df_plot_values["Perturb"], 


    fig = go.Figure()
    fig.add_trace(go.Scatter(x=df_plot_values_exp11i["Unnamed: 0"], y=df_plot_values_exp11i["Pred_acc"], mode="lines",
                    name='Pred_err 20 epochs'))
    fig.add_trace(go.Scatter(x=df_plot_values_exp11p["Unnamed: 0"], y=df_plot_values_exp11p["Pred_acc"], mode="lines",
                    name='Pred_err 5 epochs'))
    fig.add_trace(go.Scatter(x=df_plot_values_exp11q["Unnamed: 0"], y=df_plot_values_exp11q["Pred_acc"], mode="lines",
                    name='Pred_err 10 epochs'))
    fig.add_trace(go.Scatter(x=df_plot_values_exp11r["Unnamed: 0"], y=df_plot_values_exp11r["Pred_acc"], mode="lines",
                    name='Pred_err 1 epochs'))

    # fig.add_trace(go.Scatter(x=col_predictor_values, line_width=3, line_color="red", mode="lines"))

    # fig.add_trace(go.vline(x=col_generator_values, line_width=3, line_color="green", mode="lines", ))
    # fig.add_vline(x=df_retraining_iters_log_gen['Generator'], line_width=3, line_color="green")

    fig.update_layout(title="Pred error plots for 1,5,10,20 epochs experiments",
        xaxis_title="Iteration",
        yaxis_title="Value",
        legend=dict(y=0.5, traceorder='reversed', font_size=16),
        font=dict(
            family="Courier New, monospace",
            size=18,
            color="RebeccaPurple"
        ))

    return fig

app.run_server(debug=True)
# This is the main file ("TOP").
#from dbm import gnu
from os import error
import platform
import re
import sys
from statistics import mean
import pandas as pd
import numpy as np
import warnings
import argparse
import random
import subprocess
import matplotlib.pyplot as plt
import itertools
import csv
import os
import datetime
from csv import writer
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from matplotlib.figure import Figure 
 


parser = argparse.ArgumentParser(description="Generate plots")
parser.add_argument(
    "--id",
    metavar="experiment_id",
    default=0,
    help="experiment ID",
)
args = parser.parse_args()
exp_id = args.id
generate_graphs = 1 #dummy variable, used just for further processing if needed


def main(): 
    root_dir_runs = 'runs/'
    path_root = root_dir_runs + exp_id + "/"
    df_plot_data = pd.read_csv(path_root + 'plot_data.csv')
    accuracy_pred_list = df_plot_data["Pred_acc"]
    accuracy_gen_list = df_plot_data["Gen_acc"]
    perturbation_list = df_plot_data["Perturb"]
    correct_output_list = df_plot_data["Funct_to_learn"]
    y_pred_list = df_plot_data["Pred_output"]
    action_gen_list = df_plot_data["Gen_output"]
    evaluator_output_list = df_plot_data["Eval_output"]

    if (generate_graphs == 1): 
        fig, axes = plt.subplots(
                     ncols=1,
                     nrows=7,
                     figsize=(37, 30),
                     sharex=True,
                     sharey=True)

       
        ax1, ax2, ax3, ax4, ax5, ax6, ax7 = axes.flatten()
        fig = plt.figure(1)              

        ax1.plot(correct_output_list)
        ax1.set_ylabel('FN', fontsize=30.0)
        ax1.set_ylim([-1, 2])
        ax1.axhline(y=0.0, color = 'm', linestyle = '-')
        ax1.tick_params(axis='both',labelsize=18)
        ax1.set_yticks([-0.8, -0.5, 0, 0.5, 1, 1.5, 2])
        # xticks = ax1.yaxis.get_ticklabels()
        # print(xticks)
        # xticks[2].label1.set_visible(False)
        # plt.setp(ax1.get_yticklabels()[-1], visible=False)
        # for retrain_instances in retrain_gen_instances_list:
        #     ax4.axvline(x=retrain_instances, color = 'r', linestyle = '-')
        # for retrain_instances_pred in retrain_pred_instances_list:
        #     ax4.axvline(x=retrain_instances_pred, color = 'g', linestyle = "-")

        

        ax2.plot(action_gen_list)
        ax2.set_ylabel('GEN FN', fontsize=30.0)
        ax2.set_ylim([-1, 2])
        ax2.axhline(y=0.0, color = 'm', linestyle = '-')
        ax2.tick_params(axis='both',labelsize=18)
        ax2.set_yticks([-0.8, -0.5, 0, 0.5, 1, 1.5, 2])
        # labels2 = [tick.get_text() for tick in ax2.get_yticklabels()]
        # ax2.set_yticklabels(labels2[:-1])
        # plt.setp(ax2.get_yticklabels()[-2], visible=False)
        # for retrain_instances in retrain_gen_instances_list:
        #     ax6.axvline(x=retrain_instances, color = 'r', linestyle = '-')
        # for retrain_instances_pred in retrain_pred_instances_list:
        #     ax6.axvline(x=retrain_instances_pred, color = 'g', linestyle = "-")
       
        ax3.plot(accuracy_gen_list)
        ax3.set_ylabel('GEN ERR', fontsize=30.0)
        ax3.set_ylim([-1, 2])
        ax3.axhline(y=0.0, color = 'm', linestyle = '-')
        ax3.tick_params(axis='both',labelsize=18)

        # plt.setp(ax3.get_yticklabels()[-1], visible=False)
        # for retrain_instances in retrain_gen_instances_list:
        #     ax2.axvline(x=retrain_instances, color = 'r', linestyle = '-')
        # for retrain_instances_pred in retrain_pred_instances_list:
        #     ax2.axvline(x=retrain_instances_pred, color = 'g', linestyle = "-")

        ax4.plot(evaluator_output_list)
        ax4.set_ylabel('EVAL QUAL ', fontsize=30.0)
        ax4.set_ylim([-1, 2])
        ax4.axhline(y=0.0, color = 'm', linestyle = '-')
        ax4.tick_params(axis='both',labelsize=18)
        #labels4 = [tick.get_text() for tick in ax4.get_yticklabels()]
        #ax4.set_yticklabels(labels4[:-1])
        # plt.setp(ax4.get_yticklabels()[0], visible=False)

               
    
        ax5.plot(y_pred_list)
        ax5.set_ylabel('PRED QUAL', fontsize=30.0)
        ax5.set_ylim([-1, 2])
        ax5.axhline(y=0.0, color = 'm', linestyle = '-')
        ax5.tick_params(axis='both',labelsize=18)
        # plt.setp(ax5.get_yticklabels()[1], visible=False)


        ax6.plot(accuracy_pred_list)
        ax6.set_ylabel('PRED ERR', fontsize=30.0)
        ax6.set_ylim([-1, 2])
        ax6.axhline(y=0.0, color = 'm', linestyle = '-')
        ax6.tick_params(axis='both',labelsize=18)
        # plt.setp(ax6.get_yticklabels()[2], visible=False)


        ax7.plot(perturbation_list)
        ax7.set_ylabel('PERT AMPL', fontsize=30.0)
        ax7.set_ylim([-1, 2])
        ax7.axhline(y=0.0, color = 'm', linestyle = '-')
        ax7.tick_params(axis='both',labelsize=18)

        plt.subplots_adjust(hspace=0)
        ax7.set_xlabel("Iteration number", fontsize=30.0)
        # for retrain_instances in retrain_gen_instances_list:
        #     ax7.axvline(x=retrain_instances, color = 'r', linestyle = '-')
        # for retrain_instances_pred in retrain_pred_instances_list:
        #     ax7.axvline(x=retrain_instances_pred, color = 'g', linestyle = "-")

        #plt.show()

        fig.canvas.draw()
        print("Successfully regenerated plots for exp: ", exp_id)
        fig.savefig(path_root+"Figures_regenerated_" + exp_id +".svg")
        fig.savefig(path_root+"Figures_regenerated_png_" + exp_id +".png")

if __name__ == "__main__":
    main()
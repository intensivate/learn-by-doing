import os
import numpy as np
import torch

def evaluator_b09(inputs, actualP_value, adapting=0):
    #print("E3 in0:", inputs[0][0])
    #print("E3 in1:", inputs[0][1])
    target = (( (inputs[0][0])) + ( (inputs[0][1]) * 0.9)) + adapting
    #print("target:", target)
    result = torch.abs((target-actualP_value))
    #print("res:", result)
    #RMSE
    return target, result

import torch
import torch.nn as nn
import numpy as np
from torch.utils.data import DataLoader
from torch.utils.data import Subset
from Datasets.PREDDataset import PredDataset
from Datasets.GENDataset import GenDataset
from torch.optim.lr_scheduler import StepLR

from csv import writer


def train_generator(generator, epochs, device, aftertrain, index_step, cycles, init_training_set_size, nmomentum):
    generator_dataset = GenDataset(csv_file='Tgenerator.csv', root_dir='./')
    criterion = nn.MSELoss()
    #optimizer = torch.optim.SGD(generator.parameters(), lr=0.01)
    optimizer = torch.optim.SGD(generator.parameters(), lr=0.01, momentum = nmomentum)
    #scheduler = StepLR(optimizer, step_size=int(epochs/2), gamma=0.9)

    if aftertrain >= 1:
        generator.load_state_dict(torch.load('lbd_generator_model.pth'))
        
    trainset = Subset(generator_dataset, range(index_step+init_training_set_size-cycles, index_step+init_training_set_size-1))
    dataloader = DataLoader(trainset, batch_size=int(cycles), shuffle=True, num_workers=4, pin_memory=True)
    #else:
        #print("Whole dataset error")
     #   dataloader = DataLoader(generator_dataset, batch_size=int(cycles), shuffle=True, num_workers=4, pin_memory=True)
        
    for epoch in range(0, epochs):
        #print('epoch =', epoch)
        for i, samples in enumerate(dataloader):
            #print("steps train:,", i)
            inputs = samples['data_sample'].to(device)
            #print("I:", input)
            target_action = samples['target_action'].to(device)
            #print("T:", target_action)
            action = generator(inputs)
            #print("A:", action)
            loss = criterion(action, target_action)

            optimizer.zero_grad()
            loss.backward()
            optimizer.step()
        #print("lr:", scheduler.get_last_lr())
        #scheduler.step()

        # print(f'loss = {loss.item():.4f}')
        torch.save({
            'epoch': epoch,
            'model_state_dict:': generator.state_dict(),
            'optimizer_state_dict': optimizer.state_dict(),
            'loss': loss,
        }, "generator_last_checkpoint.pth")

    torch.save(generator.state_dict(), 'lbd_generator_model.pth')


def train_predictor(predictor, epochs, device, aftertrain, index_step, cycles, init_training_set_size, nmomentum):
    predictor_dataset = PredDataset(csv_file='Tpredictor.csv', root_dir='./')
    #dataloader = DataLoader(predictor_dataset, batch_size=4, shuffle=True, num_workers=4, pin_memory=True)
    criterion = nn.MSELoss()
    #optimizer = torch.optim.SGD(predictor.parameters(), lr=0.01)
    optimizer = torch.optim.SGD(predictor.parameters(), lr=0.01, momentum = nmomentum)
    #scheduler = StepLR(optimizer, step_size=int(epochs/2), gamma=0.5)

    if aftertrain >= 1:
        predictor.load_state_dict(torch.load('lbd_predictor_model.pth'))
        
    trainset_p = Subset(predictor_dataset, range(index_step+init_training_set_size-cycles, index_step+init_training_set_size-1))
    dataloader = DataLoader(trainset_p, batch_size=int(cycles), shuffle=True, num_workers=4, pin_memory=True)
    #else:
      #  print("Whole dataset training error")
        #dataloader = DataLoader(predictor_dataset, batch_size=int(cycles), shuffle=True, num_workers=4, pin_memory=True)

    for epoch in range(0, epochs): 
        #print('epoch =', epoch)
        for i, samples in enumerate(dataloader):
            #print('step = ', i)
            inputs = samples['data_sample'].to(device)
            target_score = samples['target'].to(device)

            y_pred = predictor(inputs)
            
            loss = criterion(y_pred, target_score)

            optimizer.zero_grad()
            loss.backward()
            optimizer.step()
        #scheduler.step()

        #print(f'loss = {loss.item():.4f}')
        torch.save({
            'epoch': epoch,
            'model_state_dict:': predictor.state_dict(),
            'optimizer_state_dict': optimizer.state_dict(),
            'loss': loss,
        }, "predictor_last_checkpoint.pth")

    torch.save(predictor.state_dict(), 'lbd_predictor_model.pth')


def learn_by_doing(iters, evaluator_output, prev_input, prev_y_pred, prev_perturb_action, inputs, generator, predictor, adap_perturb, device, per):
    actions_generated = 0
    predictor_generated = 0

    with torch.no_grad(): 
        #print(per)
        noise = (((torch.rand(1)*2)-1) / per).double().to(device)
        #print("inputs:", inputs)
       
        #print("noise", noise)
        action = (generator(inputs.double())).to(device)#+adapting.to(device)).to(device)
        #print("Akt:",action)
        
        perturbed_action = action+noise
        #print("PAkt:", perturbed_action)

        sample_pred = torch.cat((inputs, action), dim=1).to(device)
        #print("SP:", sample_pred)
        y_pred = predictor(sample_pred).to(device)
        #print("YPred:", y_pred)
        #print("Prev y pred:", prev_y_pred)
	
        #print("eval out:", evaluator_output)
        if torch.abs(prev_y_pred) > torch.abs(evaluator_output):
            list_data = [prev_input.cpu().numpy(), prev_perturb_action.cpu().numpy()]
            #print("print_list_data_AKT GEN", list_data)
            #print("Training data for generator:", list_data)
            #print("Gen tr data At iteration", iters)
            values = (list_data[0][0][0], list_data[0][0][1],  list_data[1][0][0])
            with open('Tgenerator.csv', 'a', newline='') as fg_object:
                writer_object = writer(fg_object)
                writer_object.writerow(values)
                actions_generated = 1
                fg_object.close()

        if iters > 0:
            list_data_pred = [prev_input.cpu().numpy(), prev_perturb_action.cpu().numpy(), evaluator_output.cpu().numpy()]
            #print("print_list_data_pred", list_data_pred)
            #print("Training data for predictor:", list_data_pred)
            #print("At iteration", iters)
            values_pred = (list_data_pred[0][0][0], list_data_pred[0][0][1], list_data_pred[1][0][0], list_data_pred[2][0][0])
            with open('Tpredictor.csv', 'a', newline='') as fp_object:
                writer_object = writer(fp_object)
                writer_object.writerow(values_pred)
                predictor_generated = 1
                fp_object.close()
            #file2_to_write_in = open(path_root + 'logs' + '.txt', "w")
            #file2_to_write_in.write("\n System: " + system)
            #file2_to_write_in.write("\n Release: " + release)
            #file2_to_write_in.write("\n Processor: " + processor)
        return inputs, y_pred, perturbed_action, action, predictor_generated, actions_generated, noise



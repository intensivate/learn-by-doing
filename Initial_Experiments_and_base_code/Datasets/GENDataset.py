import torch
import pandas as pd
import numpy as np
from torch.utils.data import Dataset

class GenDataset(Dataset):
    def __init__(self, csv_file, root_dir, transform=None):
        self.lbd_frame = pd.read_csv(csv_file)
        self.root_dir = root_dir
        self.transform = transform

    def __len__(self):
        return len(self.lbd_frame)

    def __getitem__(self, idx):

        data_sample1 = self.lbd_frame.iloc[idx, 0]
        data_sample2 = self.lbd_frame.iloc[idx, 1]
        data_sample = torch.tensor([data_sample1, data_sample2])
        #data_sample = torch.stack([data_sample1, data_sample2])

        result = torch.tensor(np.array(self.lbd_frame.iloc[idx, 2]))
        result = torch.stack([result])
        samples = {'data_sample': data_sample, 'target_action': result}
        if self.transform:
            data_sample = self.transform(data_sample)
        return samples

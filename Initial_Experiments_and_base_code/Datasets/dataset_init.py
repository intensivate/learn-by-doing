import torch
import numpy as np
import pandas as pd

x1 = np.arange(start=0, stop=0.5, step=0.0001)
x2 = np.arange(start=0.1, stop=0.6, step=0.0001)
x3 = np.zeros(len(x2))

y = x1+x2
X = np.stack((x1, x2, y, x3), axis=-1)
#print(X.shape)
df = pd.DataFrame(X)
df.to_csv("lbd_dataset1.csv", index=False)
#first two are inputs, third is result

data = pd.read_csv('lbd_dataset1.csv')
data_sample1 = np.array(data.iloc[0, 1])
data_sample2 = np.array(data.iloc[2, 1])
data_Sample = [data_sample1, data_sample2]
print(data_Sample)
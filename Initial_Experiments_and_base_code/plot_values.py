from dash import Dash, dcc, html, Input, Output
import plotly.express as px
import pandas as pd
import argparse
import os
import plotly.graph_objects as go

app = Dash(__name__)

parser = argparse.ArgumentParser(description="Plotly")
parser.add_argument(
    "--id",
    metavar="experiment_id",
    default=0,
    help="experiment ID",
)

args = parser.parse_args()
exp_id = args.id
root_dir = os.getcwd()
root_dir_runs = root_dir + "/runs/"
path_root = os.path.join(root_dir_runs, exp_id, 'plot_data.csv')
retraining_log_file_gen = os.path.join(root_dir_runs, exp_id, 'retraining_iters_gen.csv')
retraining_log_file_pred = os.path.join(root_dir_runs, exp_id, 'retraining_iters_pred.csv')


app.layout = html.Div([
    html.H4('Learn by doing experiment results'),
    dcc.Graph(id="graph")
])

@app.callback(
    Output("graph", "figure"), 
    Input("graph", "figure"))
def update_line_chart(self):
    df_plot_values = pd.read_csv(path_root)
    df_retraining_iters_log_gen = pd.read_csv(retraining_log_file_gen)
    df_retraining_iters_log_pred = pd.read_csv(retraining_log_file_pred)
    col_generator_values = df_retraining_iters_log_gen['Generator']
    col_predictor_values = df_retraining_iters_log_pred['Predictor']
    

    # fig = px.line(x=df_plot_values["Unnamed: 0"], y=[df_plot_values["Pred_acc"], df_plot_values["Gen_acc"], df_plot_values["Perturb"], 
    # df_plot_values["Funct_to_learn"], df_plot_values["Pred_output"], df_plot_values["Gen_output"], df_plot_values["Eval_output"],
    # df_plot_values["Eval_output"]])
    

    fig = go.Figure()
    fig.add_trace(go.Scatter(x=df_plot_values["Unnamed: 0"], y=df_plot_values["Pred_acc"], mode="lines",
                    name='Pred_error'))
    fig.add_trace(go.Scatter(x=df_plot_values["Unnamed: 0"], y=df_plot_values["Gen_acc"], mode="lines",
                    name='Gen_error'))
    fig.add_trace(go.Scatter(x=df_plot_values["Unnamed: 0"], y=df_plot_values["Perturb"], mode="lines",
                    name='Perturbation'))
    fig.add_trace(go.Scatter(x=df_plot_values["Unnamed: 0"], y=df_plot_values["Funct_to_learn"], mode="lines",
                    name='Function_to_learn'))
    fig.add_trace(go.Scatter(x=df_plot_values["Unnamed: 0"], y=df_plot_values["Pred_output"], mode="lines",
                    name='Pred_output'))
    fig.add_trace(go.Scatter(x=df_plot_values["Unnamed: 0"], y=df_plot_values["Gen_output"], mode="lines",
                    name='Gen_output'))                   
    fig.add_trace(go.Scatter(x=df_plot_values["Unnamed: 0"], y=df_plot_values["Eval_output"], mode="lines",
                    name='Eval_output'))
    # fig.add_trace(go.Scatter(x=col_predictor_values, line_width=3, line_color="red", mode="lines"))

    # fig.add_trace(go.vline(x=col_generator_values, line_width=3, line_color="green", mode="lines", ))
    # fig.add_vline(x=df_retraining_iters_log_gen['Generator'], line_width=3, line_color="green")

    for i in range(len(col_generator_values)):
        fig.add_vline(x=col_generator_values[i], line_width=1, line_color="green")
        # print(i)
    for i in range(len(col_predictor_values)):
        fig.add_vline(x=col_predictor_values[i], line_width=1, line_color="red")
    print("Finished updating the plots with model re-training iterations values", i)

    fig.update_layout(title="Experiment results for "+exp_id,
        xaxis_title="Iteration",
        yaxis_title="Value",
        legend=dict(y=0.5, traceorder='reversed', font_size=16),
        font=dict(
            family="Courier New, monospace",
            size=18,
            color="RebeccaPurple"
        ))

    return fig

app.run_server(debug=True)

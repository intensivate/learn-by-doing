import os
import numpy as np
import torch

def evaluator_sqrt(inputs, actualP_value, adapting=0):
    #print("E3 in0:", inputs[0][0])
    #print("E3 in1:", inputs[0][1])
    target = (inputs[0][0] +  torch.sqrt(inputs[0][1])) + adapting
    #print("target:", target)
    result = torch.abs((target-actualP_value))
    #print("res:", result)
    #RMSE
    return target, result

import torch
import torch.nn as nn
import torch.nn.functional as F


#write custom activation function or start with softmax of tanh. check which works better
#activation function heavily depends on the application


class LBDNet(nn.Module):
    def __init__(self, input_size, hidden_size, output_size):
        super(LBDNet, self).__init__()
        self.layer1 = nn.Linear(input_size, hidden_size)
        self.layer2 = nn.Linear(hidden_size, output_size)
        self.prelu=nn.PReLU()

    def forward(self, x):
        # x = F.relu(self.layer1(x))
        # return F.relu(self.layer2(x))
        # x = torch.tanh(self.layer1(x))
        # return torch.tanh(self.layer2(x))
        # x = torch.sigmoid(self.layer1(x))
        # return torch.sigmoid(self.layer2(x))
        x = self.prelu(self.layer1(x))
        return self.prelu(self.layer2(x))

# This is the main file ("TOP").
from os import error
import platform
import re
import sys
from unicodedata import name
import cpuinfo
import torch
import pandas as pd
from Measurements import input_measurement
from Nets.GeneratorNET import LBDNet
from Nets.PredictorNET import Pred_LBDNet
import numpy as np
from fixed_pert_learnbydoing import learn_by_doing, train_generator, train_predictor
import warnings
import argparse
import random
import torch.backends.cudnn as cudnn
from Evaluator_add import evaluator_add
from Evaluator_mul import evaluator_mul
import matplotlib.pyplot as plt
from Evaluator_pow import evaluator_pow
from Evaluator_a09 import evaluator_a09
from Evaluator_b09 import evaluator_b09
from Evaluator_sqrt import evaluator_sqrt
from Evaluator_logn import evaluator_logn
import itertools
import csv
import os
import datetime
from csv import writer

parser = argparse.ArgumentParser(description="Learn by doing")
parser.add_argument(
    "--seed",
    default=123,
    type=int,
    help="seed for initializing training. "
)
parser.add_argument(
    "--epochs",
    metavar="epochs",
    default=10,
    help="number of epochs",
)
parser.add_argument(
    "--train_gen",
    metavar="TRAIN_GEN",
    default=0,
    help="if train_gen=1, train generator",
)
parser.add_argument(
    "--train_pred",
    metavar="TRAIN_PRED",
    default=0,
    help="if train_pred=1, train predictor ",
)
parser.add_argument(
    "--show_graphs",
    metavar="show_graphs",
    default=1,
    help="if show=1 plot accuracy graphs ",
)
parser.add_argument(
    "--nsteps",
    metavar="NSTEPS",
    default=50000,
    help="number of steps to run learn by doing ",
)
parser.add_argument(
    "--id",
    metavar="experiment_id",
    default=0,
    help="experiment ID",
)
parser.add_argument(
    "--name",
    metavar="experiment_name",
    help="experiment name",
    default="exptest",
)
parser.add_argument(
    "--momentum",
    metavar="momentum",
    default=0.8,
    help="training momentum",
)
parser.add_argument(
    "--movavgbatch",
    metavar="movavgbatch",
    default=256,
    help="moving averge batch",
)

parser.add_argument(
    "--gpu",
    metavar="gpu",
    default=0,
    help="moving averge batch",
)
parser.add_argument(
    "--per",
    metavar="per",
    default=10,
    help="Fixed perturbation ratio termh",
)

parser.add_argument(
    "--sinfreq",
    metavar="sinfreq",
    default=1,
    help="moving averge batch",
)
args = parser.parse_args()

#Used for initial weights -- normal distribution with these parameters
mean = 0.2
stdevy = 0.1
exp_id = args.id
exp_name = str(args.name)
momentum = float(args.momentum)
mov_avg_batch = int(args.movavgbatch)
use_gpu = int(args.gpu)
frequency = float(args.sinfreq)
per = float(args.per)


#initalize the weights of a NN -- "model" is Pytorch NN
def weights_init_normal(model):
    classname = model.__class__.__name__
    if classname.find('Linear') != -1:
        model.weight.data.normal_(mean, stdevy)
        model.bias.data.fill_(0)

# used when calculating the moving average
def summa(list):
    total = 0
    for element in list:
        total = total+element
    return total

def init_csv_generator():
    file_in = 'Tgenerator_init.csv'
    file_out = 'Tgenerator.csv'

    reader_out = csv.reader(file_out)
    lines= len(list(reader_out))

    if lines <= 1000:    
        with open(file_in, "r", newline='') as f_input, \
            open(file_out, "a", newline='') as f_output:
            
            f_output.truncate(0)
            csv_input = csv.reader(f_input)
            #csv.writer(f_output).writerows(itertools.islice(csv_input, 0, 1000))
            f_output.close()
            f_input.close()
    else:
        print("Error! Generator csv file has residual rows or is deleted completely!")


def init_csv_predictor():
    file_in = 'Tpredictor_init.csv'
    file_out = 'Tpredictor.csv'

    reader_out = csv.reader(file_out)
    lines= len(list(reader_out))

    if lines <= 1000:
        with open(file_in, "r", newline='') as f_input, \
            open(file_out, "a", newline='') as f_output:

            f_output.truncate(0)
            csv_input = csv.reader(f_input)
            #csv.writer(f_output).writerows(itertools.islice(csv_input, 0, 1000))
            f_output.close()
            f_input.close()
    else:
        print("Error! Predictor csv file has residual rows or is deleted completely!")

def init_lbd(device):
    generator = LBDNet(2, 2, 1)
    generator.to(device).double()
    generator.apply(weights_init_normal)

    predictor = Pred_LBDNet(3, 2, 1)
    predictor.to(device).double()
    predictor.apply(weights_init_normal)

    if use_gpu:
        if torch.cuda.is_available():
            generator.cuda()
            predictor.cuda()
            print("Using GPU:", device, torch.cuda.is_available())
        else:
            print("Not using GPU!")

    return generator, predictor

if __name__ == '__main__':
    begin_time = datetime.datetime.now()
    if args.seed is not None:
        random.seed(args.seed)
        torch.manual_seed(args.seed)
        cudnn.deterministic = True
        warnings.warn(
            "You have chosen to seed training. "
            "This will turn on the CUDNN deterministic setting, "
            "which can slow down your training considerably! "
            "You may see unexpected behavior when restarting "
            "from checkpoints.")
    
    if use_gpu:
        device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
        print("using GPU")
    else:
        device = torch.device('cpu')
        print("Use CPU only")
    #print(device)

    if os.path.exists('lbd_predictor_model.pth'):
        os.remove('lbd_predictor_model.pth')
        print("Delete pred model")
    else:
        print("lbd_predictor_model does not exist")
        
    if os.path.exists('lbd_generator_model.pth'):
        os.remove('lbd_generator_model.pth')
        print("Delete gen model")
    else:
        print("lbd_generator_model does not exist")
        
    epochs_argument_gen = int(args.epochs)
    epochs_argument_pred = int(args.epochs)

    gen, pred = init_lbd(device)
    init_csv_predictor()
    init_csv_generator()

    total_action_generated_batch = 0
    total_predictor_generated_batch = 0
    after_train_generator = 0
    after_train_predictor = 0
    mem_gen_batch = 0
    mem_pred_batch = 0 
    difference = torch.tensor([0]).to(device)
    #init_training_set_size = 1000
    init_training_set_size = 0
    adapting = torch.tensor([0])
    root_dir_runs = 'runs/'
    path_root = root_dir_runs + exp_id + "/"
    if os.path.exists(path_root):
        print("Folder exists! Exp probably run before!")
    else:
        os.mkdir(path_root)
    
    if args.train_gen:
        gen.train()
        train_generator(gen, epochs_argument_gen)
    if args.train_pred:
        pred.train()
        train_predictor(pred, epochs_argument_pred)
    else:
        evaluator_output = torch.tensor([99]).to(device)
        prev_y_pred = torch.tensor([0]).to(device)
        prev_perturb_action = torch.tensor([0]).to(device)
        prev_input = torch.tensor([[0, 0]]).to(device)
        gen.eval()
        pred.eval()
        accuracy_gen_list = []
        accuracy_pred_list = []
        perturbation_list = []
        correct_output_list = []
        action_gen_list = []
        retrain_gen_instances_list = []
        retrain_pred_instances_list = []
        y_pred_list = []
        actual_input_1_list = []
        actual_input_2_list = []
        generator_actual_mse_list = []
        pert_action_gen_list = []
        evaluator_output_list = []
        diff_retrain_iters_gen = []
        diff_retrain_iters_pred = []
        error_list_cycles_avg = mov_avg_batch
        train_cycles_batch = 256
        actual_perturb = torch.tensor([0]).cpu()
        nr_steps = int(args.nsteps)


#This is the main loop -- run LBD for "nr_steps" iterations
        for i in range(0, nr_steps):
            gen.eval()
            pred.eval()
            # this is special case, for right after the function changes or right at the start -- increases number of epochs of training
            # On experiments where retraining is done on entire dataset, this causes more epochs across entire dataset
            # On experiments where retraining is done only on the most recent batch, then multiple epochs == repeats of training on that batch
            # so, increase number of epochs at start and right after function change, then decreates back after that
            if after_train_generator > 0:
                gen.load_state_dict(torch.load('lbd_generator_model.pth'))
            if after_train_predictor > 0:
                pred.load_state_dict(torch.load('lbd_predictor_model.pth'))

            inp = input_measurement().to(device) # inp = A, B to(device) is to send to GPU
           
            #if i >= 250000:
            #    per = float(float(per) + 0.00045)
            #elif i >= 450000:
            #    per = float(0.01)
            #else:
            #    per = float(per)
            #print(inp)

            #Now, call the learn_by_doing function, which is the control loop
            actual_input, actual_y_pred, actual_perturbed_action, actual_action, predictor_generated, action_generated, noise = \
                learn_by_doing(i, evaluator_output, prev_input, prev_y_pred, prev_perturb_action, inp, gen, pred, actual_perturb, device, per)

            #adapting = torch.tensor([0])

            #if i > int(nr_steps/3) and i <= int(2*nr_steps/3):
            if i > int(nr_steps/2):
                # adapting = 0.5*torch.sin(torch.tensor(2*np.pi*frequency*i))
                # adapting = torch.sin(torch.tensor(2*np.pi*frequency*i))
                adapting = torch.tensor([0])
                evaluator_output = evaluator_add(actual_input.to(device), actual_perturbed_action.to(device), adapting.to(device)).to(device) #evaluator result on perturbed action at i
                generator_actual_mse = evaluator_add(actual_input.to(device), actual_action.to(device), adapting.to(device)).to(device)
            else:
                adapting = torch.tensor([0])
                generator_actual_mse = evaluator_add(actual_input.to(device), actual_action.to(device), adapting.to(device)).to(device)
                evaluator_output = evaluator_add(actual_input.to(device), actual_perturbed_action.to(device), adapting.to(device)).to(device) #evaluator result on perturbed action at i
     

            # generator_actual_mse = evaluator_add(actual_input.to(device), actual_action.to(device), adapting.to(device)).to(device)
            # evaluator_output = evaluator_add(actual_input.to(device), actual_perturbed_action.to(device), adapting.to(device)).to(device)
            
            correct_target = (actual_input.cpu().sum(axis=1)+adapting.cpu())
            
            pred_mse = torch.abs(actual_y_pred - generator_actual_mse)

            actual_input_1_list.append(round(actual_input[0][0].item(), 5))
            actual_input_2_list.append(round(actual_input[0][1].item(), 5))

            accuracy_gen_list.append(round(generator_actual_mse.cpu().item(), 5))
            accuracy_pred_list.append(round(pred_mse.cpu().item(), 5))
            
            perturbation_list.append(round(noise.cpu().item(), 5))            
            correct_output_list.append(round(correct_target.cpu().item(), 5))

            y_pred_list.append(round(actual_y_pred.cpu().item(), 5))
            action_gen_list.append(round(actual_action.cpu().item(), 5))

            pert_action_gen_list.append(round(actual_perturbed_action.cpu().item(), 5))
            evaluator_output_list.append(round(evaluator_output.cpu().item(), 5))


            # This is for counting size of training batches, so far
            if action_generated:
                total_action_generated_batch += 1
                #print("Batch size waiting for re-training GENERATOR: ", total_action_generated_batch)
            if predictor_generated:
                total_predictor_generated_batch += 1
                #print("Batch size waiting for re-training PREDICTOR: ", total_predictor_generated_batch)

            # chose the batch size to be same as the length of the moving average -- just a logistic convenience
            if total_action_generated_batch == train_cycles_batch: #for a BS of 256 for AKT GEN retrain both nets
                retrain_gen_instances_list.append(i)
                total_action_generated_batch = 0
                mem_gen_batch = mem_gen_batch + 1
                index_gen = mem_gen_batch * train_cycles_batch
                gen.train()
                if len(retrain_gen_instances_list) > 2:
                    diff_batch = retrain_gen_instances_list[-1]-retrain_gen_instances_list[-2]
                    diff_retrain_iters_gen.append(diff_batch)
                #print("gen training:")
                train_generator(gen, epochs_argument_gen, device, after_train_generator, index_gen, train_cycles_batch, init_training_set_size, momentum)
                after_train_generator = after_train_generator + 1

            if total_predictor_generated_batch == train_cycles_batch: #BS 256
                retrain_pred_instances_list.append(i)
                total_predictor_generated_batch = 0
                mem_pred_batch = mem_pred_batch + 1
                index_pred = mem_pred_batch * train_cycles_batch
                pred.train()
                #print("pred training")
                if len(retrain_pred_instances_list) > 2:
                    diff_batch_pred = retrain_pred_instances_list[-1]-retrain_pred_instances_list[-2]
                    diff_retrain_iters_pred.append(diff_batch_pred)
                train_predictor(pred, epochs_argument_pred, device, after_train_predictor, index_pred, train_cycles_batch, init_training_set_size, momentum)
                after_train_predictor = after_train_predictor + 1

            #Update the "delay"
            prev_perturb_action = actual_perturbed_action
            prev_input = actual_input
            prev_y_pred = actual_y_pred

            #end of the main for loop

        mean_generat_error = (sum(accuracy_gen_list) / len(accuracy_gen_list))
        print("Testing GENERATOR MSE:", mean_generat_error)

        mean_pred_error = (sum(accuracy_pred_list) / len(accuracy_pred_list))
        print("Testing PREDICTOR MSE:", mean_pred_error)

        last_generator_error = accuracy_gen_list[-1]
        print("Last GENERATOR error value:", last_generator_error)

        last_predictor_error = accuracy_pred_list[-1]
        print("Last PREDICTOR error value:", last_predictor_error)

    end_time=datetime.datetime.now()
    total_time = (end_time - begin_time)

    data_to_save = [accuracy_pred_list, accuracy_gen_list, perturbation_list, correct_output_list, y_pred_list, action_gen_list, evaluator_output_list]
    df = pd.DataFrame({'Pred_acc': accuracy_pred_list, 'Gen_acc': accuracy_gen_list, 'Perturb': perturbation_list, 
                    'Funct_to_learn': correct_output_list, 'Pred_output': y_pred_list, 'Gen_output': action_gen_list, 'Eval_output': evaluator_output_list})
    df.to_csv(path_root + 'plot_data.csv')

    logs_df = pd.DataFrame({'input_1': actual_input_1_list, 'input_2': actual_input_2_list, 'Actual_action': action_gen_list, 
                            'A_pert_action': pert_action_gen_list, 'Pred_output': y_pred_list, 'Gen_actual_mse': accuracy_gen_list, 'Eval_output': evaluator_output_list})
    logs_df.to_csv(path_root + 'logs.csv')
    
    retrain_df_gen = pd.DataFrame({'Generator': retrain_gen_instances_list})
    retrain_df_pred = pd.DataFrame({'Predictor': retrain_pred_instances_list})

    retrain_df_gen.to_csv(path_root + 'retraining_iters_gen.csv')
    retrain_df_pred.to_csv(path_root + 'retraining_iters_pred.csv')

    diff_retrain_batches_pred = pd.DataFrame({'diff_batches': diff_retrain_iters_pred})
    diff_retrain_batches_gen = pd.DataFrame({'diff_batches': diff_retrain_iters_gen})

    diff_retrain_batches_gen.to_csv(path_root + 'diff_between_retrain_batches_gen.csv')
    diff_retrain_batches_pred.to_csv(path_root + 'diff_between_retrain_batches_pred.csv')


    my_system = platform.uname()
    system = my_system.system
    release = my_system.release
    processor = cpuinfo.get_cpu_info()['brand_raw']
    # print(f"System: ", system)
    # print(f"Release: ", release)
    # print(f"Processor: ", processor)
    print("\n Total time running the script on device: ", device)
    print(total_time)
    
    if os.path.exists(path_root):
        print("Experiments already done once. mkdir is not needed")
        current_date = datetime.datetime.today().strftime('%d-%b-%Y')
        os.rename(r'Tgenerator.csv', path_root+r'_DB_generator_' + exp_id + str(current_date) + '.csv')
        os.rename(r'Tpredictor.csv', path_root+r'_DB_predictor_' + exp_id + str(current_date) + '.csv')
    else:
        print("Directory does not exist. Creating folder for experiment: "+path_root)
        os.mkdir(path_root)
        current_date = datetime.datetime.today().strftime('%d-%b-%Y')
        os.rename(r'Tgenerator.csv', path_root+r'_DB_generator_' + exp_id + str(current_date) + '.csv')
        os.rename(r'Tpredictor.csv', path_root+r'_DB_predictor_' + exp_id + str(current_date) + '.csv')
    
    if os.path.exists(path_root+'lbd_predictor_model.pth'):
        os.rename(r'lbd_predictor_model.pth', path_root + r'lbd_predictor_model_' + exp_id + "_" + str(current_date) + '.pth')
        print("Rename pred model for exp id: " + exp_id)
    else:
        print("lbd_predictor_model does not exist")
        
    if os.path.exists(path_root+'lbd_generator_model.pth'):
        os.rename(r'lbd_generator_model.pth', path_root + r'lbd_generator_model_' + exp_id + "_" + str(current_date) + '.pth')
        print("Rename gen model for exp id: " + exp_id)
    else:
        print("lbd_generator_model does not exist")
    
    gnu_plot_script = open(path_root + 'script' + '.p', "w")
    gnu_plot_script.write('# \n')  
    gnu_plot_script.write('set terminal svg size 2880, 1920 \n')
    gnu_plot_script.write('set datafile separator "," \n')
    gnu_plot_script.write('set output "result.svg" \n')
    gnu_plot_script.write('set style line 13 lc rgb "red" \n')
    gnu_plot_script.write('set multiplot layout 7, 1 title "Experiment results" font "Arial,12" \n')
   # gnu_plot_script.write('title "Analytical plot of results for exp" "font Arial,12" ')
    gnu_plot_script.write('set title "Predictor error" \n')
    gnu_plot_script.write('set tmargin 2 \n')
    gnu_plot_script.write('set nokey \n')
    gnu_plot_script.write('set arrow from graph 0, first 0 to graph 1, first 0 nohead ls 13 \n')
    gnu_plot_script.write('plot "plot_data.csv" using 1:2 with lines lt 1\n')   
    gnu_plot_script.write('set title "Generator error" \n')
    gnu_plot_script.write('plot "plot_data.csv" using 1:3 with lines lt 1\n')
    gnu_plot_script.write('set title "Perturbation \n')   
    gnu_plot_script.write('plot "plot_data.csv" using 1:4 with lines lt 1\n')
    gnu_plot_script.write('set title "Function to learn" \n')   
    gnu_plot_script.write('plot "plot_data.csv" using 1:5 with lines lt 1\n')
    gnu_plot_script.write('set title "Predictor output" \n')   
    gnu_plot_script.write('plot "plot_data.csv" using 1:6 with lines lt 1\n')
    gnu_plot_script.write('set title "Generator output" \n')   
    gnu_plot_script.write('plot "plot_data.csv" using 1:7 with lines lt 1\n')
    gnu_plot_script.write('set title "Evaluator output" \n')   
    gnu_plot_script.write('plot "plot_data.csv" using 1:8 with lines lt 1\n')
    gnu_plot_script.write('unset multiplot \n')
    gnu_plot_script.write('# \n')  


    if use_gpu:
        file_to_write_in = open(path_root + 'gpu_run_time' + '.csv', "w")
        file_to_write_in.write("\n System: " + system)
        file_to_write_in.write("\n Release: " + release)
        file_to_write_in.write("\n Processor: " + processor)
        file_to_write_in.write("total time running script on: " + str(device))
        file_to_write_in.write("\n" + str(total_time))
        file_to_write_in.write("\n" + str(total_time))
        file_to_write_in.write("\n" + "Statistics: ")
        file_to_write_in.write("\n Mean predictor error: " + str(mean_pred_error))
        file_to_write_in.write("\n Mean generator error: " + str(mean_generat_error))
        file_to_write_in.write("\n Last predictor error value: " + str(last_predictor_error))
        file_to_write_in.write("\n Last generator error value: " + str(last_generator_error))
    else:
        file_to_write_in = open(path_root + 'cpu_run_time' + '.csv', "w")
        file_to_write_in.write("\n System: " + system)
        file_to_write_in.write("\n Release: " + release)
        file_to_write_in.write("\n Processor: " + processor)
        file_to_write_in.write("total time running script on: " + str(device))
        file_to_write_in.write("\n" + str(total_time))
        file_to_write_in.write("\n" + "Statistics: ") 
        file_to_write_in.write("\n Mean predictor error: " + str(mean_pred_error))
        file_to_write_in.write("\n Mean generator error: " + str(mean_generat_error))
        file_to_write_in.write("\n Last predictor error value: " + str(last_predictor_error))
        file_to_write_in.write("\n Last generator error value: " + str(last_generator_error))


        # generate the graphs
    if args.show_graphs:

        fig, axes = plt.subplots(
                     ncols=1,
                     nrows=7,
                     figsize=(37, 30),
                     sharex=True,
                     sharey=True)

        
        ax1, ax2, ax3, ax4, ax5, ax6, ax7 = axes.flatten()
        fig = plt.figure(1)
   
        ax6.plot(accuracy_pred_list)
        ax6.set(ylabel='Predictor error')
        ax6.set_ylim([-1, 2])
        ax6.axhline(y=0.0, color = 'm', linestyle = '-')
        # for retrain_instances in retrain_gen_instances_list:
        #     ax1.axvline(x=retrain_instances, color = 'r', linestyle = '-')
        # for retrain_instances_pred in retrain_pred_instances_list:
        #     ax1.axvline(x=retrain_instances_pred, color = 'g', linestyle = "-")
        

        ax3.plot(accuracy_gen_list)
        ax3.set(ylabel='Generator error')
        ax3.set_ylim([-1, 2])
        ax3.axhline(y=0.0, color = 'm', linestyle = '-')
        # for retrain_instances in retrain_gen_instances_list:
        #     ax2.axvline(x=retrain_instances, color = 'r', linestyle = '-')
        # for retrain_instances_pred in retrain_pred_instances_list:
        #     ax2.axvline(x=retrain_instances_pred, color = 'g', linestyle = "-")
        

        ax7.plot(perturbation_list)
        ax7.set(ylabel = 'Perturbation amplitude')
        ax7.set_ylim([-1, 2])
        ax7.axhline(y=0.0, color = 'm', linestyle = '-')
        # for retrain_instances in retrain_gen_instances_list:
        #     ax3.axvline(x=retrain_instances, color = 'r', linestyle = '-')
        # for retrain_instances_pred in retrain_pred_instances_list:
        #     ax3.axvline(x=retrain_instances_pred, color = 'g', linestyle = "-")
        

        ax1.plot(correct_output_list)
        ax1.set(ylabel='Function to learn')
        ax1.set_ylim([-1, 2])
        ax1.axhline(y=0.0, color = 'm', linestyle = '-')
        # for retrain_instances in retrain_gen_instances_list:
        #     ax4.axvline(x=retrain_instances, color = 'r', linestyle = '-')
        # for retrain_instances_pred in retrain_pred_instances_list:
        #     ax4.axvline(x=retrain_instances_pred, color = 'g', linestyle = "-")
       
    
        ax5.plot(y_pred_list)
        ax5.set(ylabel='Predictor output')
        ax5.set_ylim([-1, 2])
        ax5.axhline(y=0.0, color = 'm', linestyle = '-')
        # for retrain_instances in retrain_gen_instances_list:
        #     ax5.axvline(x=retrain_instances, color = 'r', linestyle = '-')
        # for retrain_instances_pred in retrain_pred_instances_list:
        #     ax5.axvline(x=retrain_instances_pred, color = 'g', linestyle = "-")
        

        ax2.plot(action_gen_list)
        ax2.set(ylabel='Generator output')
        ax2.set_ylim([-1, 2])
        ax2.axhline(y=0.0, color = 'm', linestyle = '-')
        # for retrain_instances in retrain_gen_instances_list:
        #     ax6.axvline(x=retrain_instances, color = 'r', linestyle = '-')
        # for retrain_instances_pred in retrain_pred_instances_list:
        #     ax6.axvline(x=retrain_instances_pred, color = 'g', linestyle = "-")
       

        ax4.plot(evaluator_output_list)
        ax4.set(ylabel='Evaluator output')
        ax4.set_ylim([-1, 2])
    
        ax7.set(xlabel="Iteration number")


        plt.subplots_adjust(hspace=0)
        ax7.axhline(y=0.0, color = 'm', linestyle = '-')
        # for retrain_instances in retrain_gen_instances_list:
        #     ax7.axvline(x=retrain_instances, color = 'r', linestyle = '-')
        # for retrain_instances_pred in retrain_pred_instances_list:
        #     ax7.axvline(x=retrain_instances_pred, color = 'g', linestyle = "-")

        #plt.show()
        #rint(exp_name)
        fig.savefig(path_root+"Figures " + exp_name +".svg")
        fig.savefig(path_root+"Figures_png " + exp_name +".png")

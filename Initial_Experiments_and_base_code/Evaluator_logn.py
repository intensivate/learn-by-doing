import os
import numpy as np
import torch

def evaluator_logn(inputs, actualP_value, adapting=0):
    #print("E3 in0:", inputs[0][0])
    #print("E3 in1:", inputs[0][1])
    target = ( (torch.log(inputs[0][0]) + torch.log(inputs[0][1]))) + adapting
    #print("target:", target)
    result = torch.abs((target-actualP_value))
    #print("res:", result)
    #RMSE
    return result

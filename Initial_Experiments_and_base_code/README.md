This folder contains the Python code that implements the basic LBD approach, as well as all results and artifacts for the first experiments.

The plan is to include the experiments and results in the first paper submitted for publication.



In order to use plotly interactive plots, please run: ``` python plot_values.py ``` and make sure you have a compatible browser: Chrome, Firefox etc.
It uses a dev Flask server to host the web app for interactive plotting.

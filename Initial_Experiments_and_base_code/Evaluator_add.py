import os
import numpy as np
import torch

def evaluator_add(inputs, actualP_value, adapting):
    target = (inputs.sum(axis=1)+adapting)
    #print("eval target insid:", target)
    result = torch.abs((target-actualP_value))
    # result = target-actualP_value
    #print("eval result insid:", result)
    #RMSE
    return target, result

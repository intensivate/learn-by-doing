import os
import numpy as np
import torch

def evaluator_mul(inputs, actualP_value, adapting=0):
    #print("E2 in0:", inputs[0][0])
    #print("E2 in1:", inputs[0][1])
    target = (inputs[0][0]**2)+(inputs[0][1]**2)+adapting
    #print("target:", target)
    result = torch.abs((target-actualP_value))
    #print("res:", result)
    #RMSE
    return target, result

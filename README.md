# Learn By Doing #

This repository contains code related to the Learn By Doing approach to self-learning / reinforcement learning

As of Dec 14, 2021, at the creation of this repo, the plans (which may or may not have come to fruition at the time you read this) are:

1. Python code that implements the basic LBD approach
2. Code that runs each of the experiments done so far
3. Results from the experiments

To check whether you have required packages installed, run (in root of repo):

$ pip -r install requirements.txt

If if you have CUDA > 11 it is ok with requirements.txt

But if you cannot install CUDA 11 or greater, then you can only have CUDA 10.2 and choose the 10.2 package from pytorch website


To run
$ cd to Initial_experiments folder
$ python3 LBD_environment.py

Note pay attention to the CSV files such that they only contain the first 1000 training examples -- when run, it adds to the training examples.
After finish running, need to delete the extra from the CSV file.

